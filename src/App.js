import React, { Component } from 'react';
import DoorLock from './components/DoorLock/DoorLock';
import {Route, Switch} from "react-router-dom";

class App extends Component {
  render() {
    return (
      <div style={{margin: "0 auto", textAlign: "center", maxWidth: "920px"}}>
        <Switch>
          <Route path="/" exact component={DoorLock}/>
        </Switch>
      </div>
    );
  }
}

export default App;
