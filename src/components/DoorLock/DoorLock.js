import React, {Component} from 'react';
import {connect} from "react-redux";
import './DoorLock.css';

const keyboard = ['7', '8', '9', '4', '5', '6', '1', '2', '3', '0'];

class DoorLock extends Component {

  sendNumber = (item) => {
    if (this.props.total.length < 4) {
      this.props.sendNumber(item);
    }
  };

  doorLock = () => {
    if (this.props.access) {
      return {css: "DoorLock-screen DoorLock-screen-access", text: "Access Granted"};
    }
    if (this.props.error) {
        return { css: "DoorLock-screen DoorLock-screen-error", text: "Access Denied"};
    }
    return {css: "DoorLock-screen", text: this.props.total.replace(/[\s\S]/g, '*')};
  };

  render() {
    return(
      <div className="DoorLock-container">
        <div className={this.doorLock().css}>
          {this.doorLock().text}</div>
        <div className="DoorLock-keyboard">
          {keyboard.map((item, index) => {
            return <span key={index} className="DoorLock-button" onClick={() => this.sendNumber(item)}>{item}</span>
          })}
          <span className="DoorLock-button" onClick={this.props.removeLastValue}>{'<'}</span>
          <span className="DoorLock-button" onClick={this.props.checkValidation}>{'E'}</span>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => state;

const mapDispatchToProps = dispatch => {
  return {
    sendNumber: (item) => dispatch({type: 'NUMBER', amount: item}),
    removeLastValue: () => dispatch({type: 'REMOVE'}),
    checkValidation: () => dispatch({type: 'CHECK'})
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(DoorLock);